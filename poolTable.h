/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <t.stibor@gsi.de>
 */

#ifndef POOLTABLE_H
#define POOLTABLE_H

#include "zfs_info.h"

/* Shared between poolTable.c and notification.c for
   accessing ZFS SNMP attributes stored in poolTable_entry. */
extern struct poolTable_entry *poolTable_head;

/* Data structure for row entry (see ZFS-MIB). */
struct poolTable_entry {

    /* 1 */
    long poolIndex;

    /* 2 */
    char poolName[ZFS_MAXNAMELEN];
    size_t poolName_len;

    /* 3 */
    char poolHealth[ZFS_MAXPROPLEN];
    size_t poolHealth_len;
    
    /* 4 */
    U64 poolAvail;

    /* 5 */
    U64 poolUsed;

    /* 6 */
    U64 poolTotal;

    /* 7 */
    char poolCompressRatio[ZFS_MAXPROPLEN];
    size_t poolCompressRatio_len;

    /* 8 */
    char poolDedupRatio[ZFS_MAXPROPLEN];
    size_t poolDedupRatio_len;

    /* Illustrate using a simple linked list */
    int   valid;
    struct poolTable_entry *next;
};

void init_poolTable(void);
void initialize_table_poolTable(void);
Netsnmp_Node_Handler poolTable_handler;
Netsnmp_First_Data_Point  poolTable_get_first_data_point;
Netsnmp_Next_Data_Point   poolTable_get_next_data_point;

void close_libzfs();
unsigned int get_n_pools();
void update_health_status(struct poolTable_entry *table_entry);

/* column number definitions for table poolTable */
#define COLUMN_POOLINDEX		1
#define COLUMN_POOLNAME			2
#define COLUMN_POOLHEALTH		3
#define COLUMN_POOLAVAIL		4
#define COLUMN_POOLUSED			5
#define COLUMN_POOLTOTAL		6
#define COLUMN_POOLCOMPRESSRATIO	7
#define COLUMN_POOLDEDUPRATIO	        8

#endif /* POOLTABLE_H */
