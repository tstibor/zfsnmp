/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <t.stibor@gsi.de>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include "zfs_info.h"

static unsigned int _n_pools = 0;
static char _zfs_pool_name[POOLS_MAX][ZFS_MAXNAMELEN];

static int zp_collect_names(zpool_handle_t *zpool_handle, void *v)
{
    if (_n_pools < POOLS_MAX) {
	strncpy(_zfs_pool_name[_n_pools], zpool_get_name(zpool_handle), ZFS_MAXNAMELEN);
	_n_pools++;
    } else {
	snmp_log(LOG_WARNING, "warning: number of pools is greater than POOLS_MAX, not all pools are considered\n");
	return ERROR;
    }

    return SUCCESS;
}

int get_zpool_prop(libzfs_handle_t *libzfs_handle, const char const *pool_name,
		   zpool_prop_t prop, char result[ZFS_MAXPROPLEN])
{
    zpool_handle_t *zpool_handle;
    int rc;

    if (libzfs_handle == NULL) {
	snmp_log(LOG_ERR, "error: libzfs_handle is NULL pointer.");
	return ERROR;
    }

    zpool_handle = zpool_open_canfail(libzfs_handle, pool_name);
    if (zpool_handle == NULL) {
	snmp_log(LOG_ERR, "error: zpool_open_canfail(%p, %s)\n", libzfs_handle, pool_name);
	return ERROR;
    }

    rc = zpool_get_prop(zpool_handle, prop, result, ZFS_MAXPROPLEN, NULL);
    if (rc != SUCCESS) {
	snmp_log(LOG_ERR, "error: zpool_get_prop(%p, %s), rc = %d\n", zpool_handle, result, rc);
	zpool_close(zpool_handle);
	return ERROR;
    }

    zpool_close(zpool_handle);
    return SUCCESS;
}

int get_zpool_prop_int(libzfs_handle_t *libzfs_handle, const char const *pool_name,
		       zpool_prop_t prop, uint64_t *result)
{
    zpool_handle_t *zpool_handle;

    if (libzfs_handle == NULL) {
	snmp_log(LOG_ERR, "error: libzfs_handle is NULL pointer\n");
	return ERROR;
    }

    zpool_handle = zpool_open_canfail(libzfs_handle, pool_name);
    if (zpool_handle == NULL) {
	snmp_log(LOG_ERR, "error: zpool_open_canfail(%p, %s)\n", libzfs_handle, pool_name);
	return ERROR;
    }

    *result = zpool_get_prop_int(zpool_handle, prop, NULL);

    zpool_close(zpool_handle);
    return SUCCESS;
}

int get_zfs_prop(libzfs_handle_t *libzfs_handle, const char const *pool_name, 
		 zfs_prop_t prop, char result[ZFS_MAXPROPLEN])
{
    zfs_handle_t *zfs_handle;
    char buf[ZFS_MAXPROPLEN];
    zprop_source_t sourcetype;
    char source[ZFS_MAXNAMELEN];

    if (libzfs_handle == NULL) {
	snmp_log(LOG_ERR, "error: libzfs_handle is NULL pointer\n");
	return ERROR;
    }

    zfs_handle = zfs_open(libzfs_handle, pool_name, ZFS_TYPE_FILESYSTEM);
    if (zfs_handle == NULL) {
	snmp_log(LOG_ERR, "error: zfs_open(%p, %s, ZFS_TYPE_FILESYSTEM)\n", libzfs_handle, pool_name);
	return ERROR;
    }

    if (zfs_prop_get(zfs_handle, prop, buf,
		     sizeof(buf), &sourcetype, source,
		     sizeof(source), 1) != 0) {
	snmp_log(LOG_ERR, "error: zfs_prop_get(%p, ZFS_PROP_AVAILABLE, ...)\n", zfs_handle);
	zfs_close(zfs_handle);
	return ERROR;
    }

    strncpy(result, buf, ZFS_MAXPROPLEN);

    zfs_close(zfs_handle);
    return SUCCESS;
}

int discover_pool_names(libzfs_handle_t *libzfs_handle, char zfs_pool_name[POOLS_MAX][ZFS_MAXNAMELEN],
			unsigned int *n_pools)
{
    if (libzfs_handle == NULL) {
	snmp_log(LOG_ERR, "error: discover_pool_names(...) *libzfs_handle is NULL pointer\n");
	return ERROR;
    }

    zpool_iter(libzfs_handle, zp_collect_names, NULL);
    *n_pools = _n_pools;
    memcpy(zfs_pool_name, _zfs_pool_name, POOLS_MAX * ZFS_MAXNAMELEN);

    return SUCCESS;
}
