/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <t.stibor@gsi.de>
 */

#include <string.h>
#include <errno.h>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

#define VERSION "0.2"

#define MAX_OID_I 256
#define OID_POOL_INDEX         "1.3.6.1.4.1.43231.1.1.1.1.1.1"
#define OID_POOL_NAME          "1.3.6.1.4.1.43231.1.1.1.1.1.2"
#define OID_POOL_HEALTH        "1.3.6.1.4.1.43231.1.1.1.1.1.3"
#define OID_POOL_AVAIL         "1.3.6.1.4.1.43231.1.1.1.1.1.4"
#define OID_POOL_USED          "1.3.6.1.4.1.43231.1.1.1.1.1.5"
#define OID_POOL_TOTAL         "1.3.6.1.4.1.43231.1.1.1.1.1.6"
#define OID_POOL_COMPRESSRATIO "1.3.6.1.4.1.43231.1.1.1.1.1.7"
#define OID_POOL_DEDUPRATIO    "1.3.6.1.4.1.43231.1.1.1.1.1.8"

#define MAX_STRING_LEN 256
#define POOLS_MAX 512

#define COMMUNITY "public"

#define KiB 1024		/* 2^10 */
#define MiB 1048576		/* 2^10 * KiB */
#define GiB 1073741824		/* 2^10 * MiB */
#define TiB 1.099512e+12	/* 2^10 * GiB */
#define PiB 1.1259e+15          /* 2^10 * TiB */

#define ERROR -1
#define SUCCESS 0

/** Array of OIDs to query. */
struct oid {
    const char *name;
    oid Oid[MAX_OID_LEN];
    size_t len;
} oids[] = {
  { OID_POOL_INDEX },         /** Pool index. */
  { OID_POOL_NAME },          /** Pool name. */
  { OID_POOL_HEALTH },        /** Pool health. */
  { OID_POOL_AVAIL },         /** Pool avail space. */
  { OID_POOL_USED },          /** Pool used space. */
  { OID_POOL_TOTAL },         /** Pool total space. */
  { OID_POOL_COMPRESSRATIO }, /** Pool compression ratio. */
  { OID_POOL_DEDUPRATIO },    /** Pool deduplication ratio. */
  { NULL }
};

/** Encapsulated data structure for storing ZFS pool information
    queried from SNMP sub-agent and presented to the user. */
typedef struct pool {
    unsigned int index;
    char name[MAX_STRING_LEN];
    char health[MAX_STRING_LEN];
    unsigned long long avail;
    unsigned long long used;
    unsigned long long total;
    char compressratio[MAX_STRING_LEN];
    char dedupratio[MAX_STRING_LEN];
}  pool_t; 

/** Main data structure comprises ZFS pool information
    and host information such as hostname and 
    whether SNMP query was successful. */
typedef struct {
    const char *name;
    const char *community;
    unsigned char query_failed;
    pool_t pool[POOLS_MAX];
    unsigned int n_pools;
} host_zfsnmp_info_t;

/** Array of pointers to main data structure comprises ZFS pool information
    and host information. */
host_zfsnmp_info_t **host_zfsnmp_info = NULL;

/** Total number of hosts queried. */
unsigned int n_hosts = 0;

/** Boolean variable set to 1 if any queried host had
    an unhealthy ZFS pool. */
unsigned char has_health_problem = 0;

/** Total number of hosts where queried failed.  */
unsigned int n_host_failed = 0;

/** Flag variable for argument '-p'. */
unsigned char problem_flag = 0;

/** Flag variable for argument '-s'. */
unsigned char summary_flag = 0;

/** Initialize and parse OIDs. If \a read_objid(...) fails, then
 *  \a exit(1) is called and the process terminates right at the beginning.
 */
void initialize()
{
  struct oid *op = oids;
  
  /* Initialize library. */
  init_snmp("zfsnmp");

  /* Parse the oids. */
  while (op->name) {
      op->len = sizeof(op->Oid)/sizeof(op->Oid[0]);

      if (!read_objid(op->name, op->Oid, &op->len)) {
	  snmp_perror("read_objid");
	  exit(1);
      }
      op++;
  }
}

/**
 * Format byte size in human readable in units KiB, MiB, GiB, etc.
 *
 * @param[in] size_bytes size in bytes.
 * @param[out] size_human_readable modified byte size with appended unit
 */
void readable_bytes(unsigned long long size_bytes, char *size_human_readable)
{
    if (size_human_readable == NULL)
	return;

    if (size_bytes < MiB)
	sprintf(size_human_readable, "%4.2f KiB", size_bytes / (float)KiB);
    else if (size_bytes < GiB)
	sprintf(size_human_readable, "%4.2f MiB", size_bytes / (float)MiB);
    else if (size_bytes < TiB)
	sprintf(size_human_readable, "%4.2f GiB", size_bytes / (float)GiB);
    else if (size_bytes < PiB)
	sprintf(size_human_readable, "%4.2f TiB", size_bytes / (float)TiB);
    else
	sprintf(size_human_readable, "%4.2f PiB", size_bytes / (float)PiB);
}

/**
 * Helper function to remove characters such as ':' and '"' in SNMP response value.
 *
 * @param[in] in_value SNMP response value.
 * @param[out] out_value modified value where characters such as ':' and '"' are removed.
 *
 * @return #ERROR if character remove failed, otherwise #SUCCESS.
 */
int extract_result(const char *in_value, char *out_value)
{
    if (out_value == NULL || in_value == NULL)
	return ERROR;

    for (unsigned int i = 0; i < strlen(in_value); i++) {
	if (in_value[i] == ':') {
	    strncpy(out_value, in_value + (i + 2), strlen(in_value) - (i + 2));
	    out_value[strlen(in_value) - (i + 2)] = '\0';

	    if (out_value[0] == '"') {
		memmove(out_value, out_value + 1, strlen(in_value) - 1);
		out_value[strlen(out_value)-1] = '\0';
	    }
	    return SUCCESS;
	}
    }
    return ERROR;
}

/**
 * Fill globally declared #host_zfsnmp_info which comprises ZFS pool information
 * and host information with received SNMP response data.
 *
 * @param[in] status result from \a snmp_synch_response contains value such as e.g. \a STAT_SUCCESS.
 * @param[in] sp pointer to current \a snmp_session.
 * @param[in] pdu pointer to current \a snmp_pdu.
 * @param[in] oid_name OID in string representation.
 * @param[in] pool_index index for addressing pool.
 * @param[in] host_index index for addressing host in #host_zfsnmp_info.
 *
 * @return #ERROR if filling fields in struct fails, otherwise #SUCCESS.
 */
int fill_structs(int status, struct snmp_session *sp, 
		 struct snmp_pdu *pdu, const char *oid_name, 
		 unsigned int pool_index, unsigned int host_index)
{
    char buf[1024];
    struct variable_list *vp;
    int ix;
    char buf_value[MAX_STRING_LEN];

    switch (status) {
    case STAT_SUCCESS:
	vp = pdu->variables;
	if (pdu->errstat == SNMP_ERR_NOERROR) {
	    while (vp) {
		if ((vp->type != SNMP_ENDOFMIBVIEW) &&
		    (vp->type != SNMP_NOSUCHOBJECT) &&
		    (vp->type != SNMP_NOSUCHINSTANCE)) {

		    snprint_variable(buf, sizeof(buf), vp->name, vp->name_length, vp);
		    snprint_value(buf, sizeof(buf), vp->name, vp->name_length, vp);
		    extract_result(buf, buf_value);
		    
		    if (strcmp(oid_name, OID_POOL_INDEX) == 0) { /* Pool index. */
			host_zfsnmp_info[host_index]->pool[pool_index-1].index = atoi(buf_value);
			host_zfsnmp_info[host_index]->n_pools++;
		    }
		    else if (strcmp(oid_name, OID_POOL_NAME) == 0) /* Pool name. */
			strcpy(host_zfsnmp_info[host_index]->pool[pool_index-1].name, buf_value);
		    else if (strcmp(oid_name, OID_POOL_HEALTH) == 0) /* Pool health. */
			strcpy(host_zfsnmp_info[host_index]->pool[pool_index-1].health, buf_value);
		    else if (strcmp(oid_name, OID_POOL_AVAIL) == 0) /* Pool avail space. */
			host_zfsnmp_info[host_index]->pool[pool_index-1].avail = atoi(buf_value);
		    else if (strcmp(oid_name, OID_POOL_USED) == 0) /* Pool used space. */
			host_zfsnmp_info[host_index]->pool[pool_index-1].used = atoi(buf_value);
		    else if (strcmp(oid_name, OID_POOL_TOTAL) == 0) /* Pool total space. */
			host_zfsnmp_info[host_index]->pool[pool_index-1].total = atoi(buf_value);
		    else if (strcmp(oid_name, OID_POOL_COMPRESSRATIO) == 0) /* Pool compressratio. */
			strcpy(host_zfsnmp_info[host_index]->pool[pool_index-1].compressratio, buf_value);
		    else if (strcmp(oid_name, OID_POOL_DEDUPRATIO) == 0) /* Pool dedupratio. */
			strcpy(host_zfsnmp_info[host_index]->pool[pool_index-1].dedupratio, buf_value);
		}
		vp = vp->next_variable;
	    } /* End while */
	    return SUCCESS;
	}
	else {
	    for (ix = 1; vp && ix != pdu->errindex; vp = vp->next_variable, ix++);
	    if (vp) 
		snprint_objid(buf, sizeof(buf), vp->name, vp->name_length);
	    else 
		strcpy(buf, "(none)");
	    fprintf(stdout, "%s: %s: %s\n", sp->peername, buf, snmp_errstring(pdu->errstat));
	}
	return ERROR;
    case STAT_TIMEOUT:
	fprintf(stderr, "%s: timeout\n", sp->peername);
	return ERROR;
    case STAT_ERROR:
	fprintf(stderr, "%s: snmp error\n", sp->peername);
	return ERROR;
    }
    return ERROR;
}

/**
 * Print tabular formatted result of ZFS pool information.
 *
 * @param[in] host_index index for addressing host in \a host_zfsnmp_info.
 * @param[out] summary_avail available space after summing up \a host_index many hosts.
 * @param[out] summary_used used space after summing up \a host_index many hosts.
 * @param[out] summary_total total space after summing up \a host_index many hosts.
 */
void print_result(unsigned int host_index,
		  float *summary_avail,
		  float *summary_used,
		  float *summary_total)
{
    unsigned int shift_right = (unsigned int)strlen(host_zfsnmp_info[host_index]->name)/2;
    char padding[256];
    unsigned char target_len;
    int pad_len;

    if (!problem_flag && !summary_flag) {
	target_len = 10;
	sprintf(padding, "%s", (char*)memset(memset(alloca(256), '\0', 256), ' ', 20));
	pad_len = target_len - strlen(host_zfsnmp_info[host_index]->name);
	printf("\n%s%*.*s [%4d/%4d]\n", 
	       host_zfsnmp_info[host_index]->name, pad_len, pad_len, padding, 
	       host_index + 1, n_hosts);

	sprintf(padding, "%s", (char*)memset(memset(alloca(256), '\0', 256), '-', 100));
	printf("%*s %9s %21s %12s %9s %15s %17s %6s\n", shift_right, "|", 
		         "name", "health", "available", "used", "total", "compress", "dedup");
	printf("%*s-%4s\n", shift_right, "+", padding);
    }

    sprintf(padding, "%s", (char*)memset(memset(alloca(256), '\0', 256), ' ', 20));
    char as_str[256];

    /* Iterate over all pools. */
    for (unsigned int i = 0; i < host_zfsnmp_info[host_index]->n_pools; i++) {
	if (!problem_flag && !summary_flag) {

	    printf("%*s-%d%4s", shift_right, "+", host_zfsnmp_info[host_index]->pool[i].index,"");

	    target_len = 20;
	    pad_len = target_len - strlen(host_zfsnmp_info[host_index]->pool[i].name);
	    printf("%s%*.*s", host_zfsnmp_info[host_index]->pool[i].name, pad_len, pad_len, padding);

	    target_len = 10;
	    pad_len = target_len - strlen(host_zfsnmp_info[host_index]->pool[i].health);
	    printf("%s%*.*s", host_zfsnmp_info[host_index]->pool[i].health, pad_len, pad_len, padding);

	    target_len = 15;
	    readable_bytes(host_zfsnmp_info[host_index]->pool[i].avail, as_str);
	    pad_len = target_len - strlen(as_str);
	    printf("%s%*.*s", as_str, pad_len, pad_len, padding);

	    readable_bytes(host_zfsnmp_info[host_index]->pool[i].used, as_str);
	    pad_len = target_len - strlen(as_str);
	    printf("%s%*.*s", as_str, pad_len, pad_len, padding);

	    readable_bytes(host_zfsnmp_info[host_index]->pool[i].total, as_str);
	    pad_len = target_len - strlen(as_str);
	    printf("%s%*.*s", as_str, pad_len, pad_len, padding);

	    target_len = 10;
	    pad_len = target_len - strlen(host_zfsnmp_info[host_index]->pool[i].compressratio);
	    printf("%s%*.*s", host_zfsnmp_info[host_index]->pool[i].compressratio, pad_len, pad_len, padding );

	    pad_len = target_len - strlen(host_zfsnmp_info[host_index]->pool[i].dedupratio);
	    printf("%s%*.*s\n", host_zfsnmp_info[host_index]->pool[i].dedupratio, pad_len, pad_len, padding );
	}
	else {
	    if (strncmp(host_zfsnmp_info[host_index]->pool[i].health, "ONLINE", strlen("ONLINE")) != 0) {
		if (!summary_flag)
		    printf("host: '%s' has one or several unhealthy pools and functioning can be compromised!\n", host_zfsnmp_info[host_index]->name);
		has_health_problem = 1;
	    }
	}
	*summary_avail += (float)host_zfsnmp_info[host_index]->pool[i].avail; 
	*summary_used += (float)host_zfsnmp_info[host_index]->pool[i].used;
	*summary_total += (float)host_zfsnmp_info[host_index]->pool[i].total;
    }
}

/**
 * Perform synchronous SNMP query to fetch ZFS pool information.
 */
void synchronous_query()
{
    unsigned int oid_i = 1;
    float summary_avail = 0;
    float summary_used = 0;
    float summary_total = 0;

    struct timespec start_time, end_time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);

    /* Iterate over all hosts. */
    for (unsigned int host_index = 0; host_index < n_hosts; host_index++) {

	struct snmp_session ss, *sp;
	struct oid *op;
	struct oid *op_i;

	snmp_sess_init(&ss);
	ss.version = SNMP_VERSION_2c;
	ss.peername = strdup(host_zfsnmp_info[host_index]->name);
	ss.community = (u_char *)strdup(host_zfsnmp_info[host_index]->community);
	ss.community_len = strlen((char *)ss.community);

	if (!(sp = snmp_open(&ss))) {

	    /* Connect failed for this host. */
	    host_zfsnmp_info[host_index]->query_failed = 1;
	    n_host_failed++;
	    printf("\ncannot connect to host: %s\n", host_zfsnmp_info[host_index]->name);
	    goto next_host;
	}
	/* Iterate over all OIDs */
	for (op = oids; op->name; op++) {
	    struct snmp_pdu *req, *resp;
	    int status;

	    op_i = malloc(sizeof(struct oid));
	    memcpy(op_i, op, sizeof(struct oid));

	    op_i->len++;
	    while (oid_i <= MAX_OID_I) {
		op_i->Oid[op_i->len-1] = oid_i;

		req = snmp_pdu_create(SNMP_MSG_GET);
		snmp_add_null_var(req, op_i->Oid, op_i->len);

		status = snmp_synch_response(sp, req, &resp);
		if (status != STAT_SUCCESS) { /* If we get STAT_ERROR or STAT_TIMEOUT, then goto next host. */
		    oid_i = 1;
		    snmp_free_pdu(resp);
		    if (op_i)
			free(op_i);
		    /* Query failed for this host. */
		    host_zfsnmp_info[host_index]->query_failed = 1;
		    n_host_failed++;
		    printf("\ncannot snmp query to host: %s\n", host_zfsnmp_info[host_index]->name);
		    goto next_host;
		}
		const struct variable_list *vars = resp->variables;
		if (resp->errstat == SNMP_ERR_NOERROR) {
		    while (vars) {
			/* See include/net-snmp/library/snmp.h for SNMP_... */
			if ((vars->type == SNMP_ENDOFMIBVIEW) ||
			    (vars->type == SNMP_NOSUCHOBJECT) ||
			    (vars->type == SNMP_NOSUCHINSTANCE)) {
		      
			    oid_i = MAX_OID_I + 1;
			}
			vars = vars->next_variable;
		    }
		}
		if (fill_structs(status, sp, resp, op->name, oid_i, host_index) == ERROR)
		    break;

		snmp_free_pdu(resp);
		oid_i++;
	    } /* End while sub-OID */
	    oid_i = 1;
	    free(op_i);
	} /* End for iterate over OIDs. */
	print_result(host_index,
		     &summary_avail,
		     &summary_used,
		     &summary_total);
    next_host:
	snmp_close(sp);
    } /* End for iterate over all hosts. */

    clock_gettime(CLOCK_MONOTONIC, &end_time);
    double total_time = (end_time.tv_sec * 1e+09 + end_time.tv_nsec) - (start_time.tv_sec * 1e+09 + start_time.tv_nsec);

    /* Print summary. */
    if (!problem_flag) {
	char as_str[256];
	printf("\nsummary: %d host(s) queried in %3.2f secs with overall capacities (summarized over all hosts)\n", n_hosts - n_host_failed, total_time / 1e+09);
	readable_bytes(summary_avail, as_str);
	printf("available space: %10s\n", as_str);
	readable_bytes(summary_used, as_str);
	printf("     used space: %10s\n", as_str);
	readable_bytes(summary_total, as_str);
	printf("    total space: %10s\n", as_str);
	printf("\nhost(s) failed to query: %d\n", n_host_failed);
	for (unsigned int i = 0; i < n_hosts; i++) {
	    if (host_zfsnmp_info[i]->query_failed == 1)
		printf("%s\n", host_zfsnmp_info[i]->name);
	}
    } else {
	if (!has_health_problem)
	    printf("\nsummary: %d host(s) queried in %3.2f secs, no problems found\n", n_hosts, total_time / 1000000000);
    }
    
}

/**
 * Print usage message and syntax.
 *
 * @param[in] prgname name of executable programm.
 */
void usage(const char *prgname)
{
    printf("syntax: %s\n", prgname);
    printf("        <hostname1> <hostname2> ... specify hostnames or IP addresses\n");
    printf("        -f <hostfile> specify file where each row contains a hostname or IP address ('#' denotes a comment and the row is ignored)\n");
    printf("        -s [optional] parameter to summarize only the total capacity statistics\n");
    printf("        -p [optional] parameter named 'problem' to quickly see whether unhealthy ZFS pools exist\n\n");
    printf("example: %s -f zfsnmphosts.txt -p\n", prgname);
    printf("example: %s 10.10.2.17 lx-zfs01.gsi.de lx-zfs73.gsi.de\n", prgname);
    printf("\nversion %s, written by <t.stibor@gsi.de>, HPC Group at GSI, 2014\n", VERSION);
    exit(1);
}

/**
 * Free memory of #host_zfsnmp_info.
 */
void cleanup_hosts()
{
    for (unsigned int h = 0; h < n_hosts; h++)
	free(host_zfsnmp_info[h]);
    free(host_zfsnmp_info);
}

/**
 * Add hostname and community sting to #host_zfsnmp_info.
 * Memory is allocated internally this function and #n_hosts is
 * increased whenever a host is added.
 *
 * @param[in] name hostname of SNMP machine to be queried.
 * @param[in] community of SNMP machine to be queried.
 */
void add_host(const char *name, const char *community)
{
    host_zfsnmp_info = realloc(host_zfsnmp_info, (n_hosts + 1) * sizeof(host_zfsnmp_info_t*));

    if (host_zfsnmp_info == NULL) {
    	fprintf(stderr, "error: cannot allocate enough memory: %s\n", strerror(errno));
	cleanup_hosts();
    	exit(2);
    }

    host_zfsnmp_info[n_hosts] = malloc(sizeof(host_zfsnmp_info_t));
    if (host_zfsnmp_info[n_hosts] == NULL) {
    	fprintf(stderr, "error: cannot allocate enough memory: %s\n", strerror(errno));
	cleanup_hosts();
    	exit(2);
    }

    host_zfsnmp_info[n_hosts]->name = strdup(name);
    host_zfsnmp_info[n_hosts]->community = strdup(community);
    n_hosts++;
}

/**
 * Read file (specified by parameter -f <filename>) where
 * each row contains a hostname or IP address of SNMP host
 * to be queried.
 *
 * @param[in] filename specifies the file to be read.
 */
void read_hostfile(const char *filename)
{
    FILE *file;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
 
    file = fopen(filename, "r");
    if (file == NULL) {
	fprintf(stderr, "error fopen: '%s' %s\n", filename, strerror(errno));
	exit(1);
    }

    while ((read = getline(&line, &len, file)) != -1) {
	/* If line starts with '#' or string length is smaller than 2, skip those line. */
	if (!(strncmp(line, "#", 1) == 0 || strlen(line) < 2)) {
	    line[strlen(line)-1] = '\0';
	    add_host(line, COMMUNITY);
	}
    }
    fclose(file);
}

/**
 * Main function.
 *
 * @param[in] argc number of arguments.
 * @param[in] argv argument list.
 *
 * @return SUCCESS if no error occured, otherwise a value not equal 0 is retured.
 */
int main(int argc, char **argv)
{
    int c;
    char *hostfile = NULL;

    if (argc < 2)
	usage(argv[0]);

    while ((c = getopt (argc, argv, "f:ps")) != -1)
	switch (c)
	{
	case 'p':
	    problem_flag = 1;
	    break;
	case 'f':
             hostfile = optarg;
             break;
	case 's':
	    summary_flag = 1;
	    break;
	default:
	    usage(argv[0]);
	    break;
	}

    /* Hostfile is specified, so read it. */
    if (hostfile != NULL) {
	read_hostfile(hostfile);
    } else {
	/* Otherwise read hostname or IP address as arguments. */
	for (int i = optind; i < argc; i++) {
	    add_host(argv[i], COMMUNITY);
	}
    }

    initialize();
    synchronous_query();
    cleanup_hosts();

    return SUCCESS;
}
