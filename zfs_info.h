/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <t.stibor@gsi.de>
 */

#include <libzfs.h>

#define ERROR -1
#define SUCCESS 0
#define POOLS_MAX 512

int get_zpool_prop(libzfs_handle_t *libzfs_handle, const char const *pool_name,
		   zpool_prop_t prop, char result[ZFS_MAXPROPLEN]);
int get_zpool_prop_int(libzfs_handle_t *libzfs_handle, const char const *pool_name,
		       zpool_prop_t prop, uint64_t *result);
int get_zfs_prop(libzfs_handle_t *libzfs_handle, const char const *pool_name, 
		 zfs_prop_t prop, char result[ZFS_MAXPROPLEN]);
int discover_pool_names(libzfs_handle_t *libzfs_handle, char zfs_pool_name[POOLS_MAX][ZFS_MAXNAMELEN], unsigned int *n_pools);
