/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <t.stibor@gsi.de>
 */

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "poolTable.h"
#include "notification.h"

/*
 * Initialization routine to get called, note 
 * function name must match init_FILENAME() 
 */
void init_notification(int notify_each_sec)
{
    DEBUGMSGTL(("notification",
                "initializing (setting callback alarm)\n"));
    snmp_alarm_register(notify_each_sec,   /* seconds */
                        SA_REPEAT,         /* repeat (every NOTIFY_EACH_SEC seconds). */
                        send_notification, /* our callback */
                        NULL);             /* no callback data needed */
}

void send_notification(unsigned int clientreg, void *clientarg)
{
    static u_long count = 0;

    /* snmpTrapOID (see snmptranslate -m SNMPv2-MIB -On -Td 1.3.6.1.6.3.1.1.4.1.0). */
    oid objid_snmptrap[] = {1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    size_t objid_snmptrap_len = OID_LENGTH(objid_snmptrap);

    /* ZFS-MIB::poolHealthNotification. */
    oid notification_oid[] = {1, 3 ,6 ,1, 4, 1, 43231, 1, 1, 1, 2 };
    size_t  notification_oid_len = OID_LENGTH(notification_oid);

    /* ZFS-MIB::poolName
     * Remove 0 and append .1, .2, ... */
    oid      pool_name_oid[]   = {1, 3 ,6 ,1, 4, 1, 43231, 1, 1, 1, 1, 1, 2, 0}; 
    size_t   pool_name_oid_len = OID_LENGTH(pool_name_oid);

    /* ZFS-MIB::poolHealth
    * Remove 0 and append .1, .2, ... */
    oid      pool_health_oid[] = {1, 3 ,6 ,1, 4, 1, 43231, 1, 1, 1, 1, 1, 3, 0};
    size_t   pool_health_oid_len = OID_LENGTH(pool_health_oid);

    /* sysName (hostname) */
    oid      hostname_oid[] = {1, 3, 6, 1, 2, 1, 1, 5};
    size_t   hostname_oid_len = OID_LENGTH(hostname_oid);

    /* Is there any ZFS pool having health status other than 'ONLINE'? */
    if (poolTable_head != NULL) {
	struct poolTable_entry *ptr = poolTable_head;
	unsigned int i = get_n_pools();
	netsnmp_variable_list *notification_vars = NULL;
	char hostname[256];

	/* Iterate over all poolTable_entries and get current health status. */
	while (ptr != NULL) {

	    pool_name_oid[pool_name_oid_len - 1] = i;
	    pool_health_oid[pool_health_oid_len - 1] = i;

	    /* First get actual health status via libzfs. */
	    update_health_status(ptr);

	    if (ptr->poolHealth != NULL && strcmp(ptr->poolHealth, "ONLINE") != 0) {
		/*
		 * Trap definition object.
		 */
		snmp_varlist_add_variable(&notification_vars,
					  objid_snmptrap, objid_snmptrap_len,
					  ASN_OBJECT_ID,
					  (u_char *)notification_oid,
					  notification_oid_len * sizeof(oid));
		/*
		 * Send hostname to trap sink.
		 */
		if (gethostname(hostname, 256) == -1)
		    strcpy(hostname, "(unknown))");

		snmp_varlist_add_variable(&notification_vars,
					  hostname_oid, hostname_oid_len,
					  ASN_OCTET_STR,
					  (const u_char *)hostname, strlen(hostname));
		/*
		 * Send pool name to trap sink.
		 */
		snmp_varlist_add_variable(&notification_vars,
					  pool_name_oid, pool_name_oid_len,
					  ASN_OCTET_STR,
					  (const u_char *)ptr->poolName, ptr->poolName_len);
		/*
		 * Send health status to trap sink.
		 */
		snmp_varlist_add_variable(&notification_vars,
					  pool_health_oid, pool_health_oid_len,
					  ASN_OCTET_STR,
					  (const u_char *)ptr->poolHealth, ptr->poolHealth_len); 

		/* Send the trap out.  This will send it to all registered
		 * receivers (see the "SETTING UP TRAP AND/OR INFORM DESTINATIONS"
		 * section of the snmpd.conf manual page. 
		 */
		count++;
		DEBUGMSGTL(("notification", "sending trap %ld\n", count));
		send_v2trap(notification_vars);

		/* Free the created notification variable list */
		DEBUGMSGTL(("notification", "cleaning up\n"));
		snmp_free_varbind(notification_vars);
	    }
	    i--;
	    ptr = ptr->next;
	}
    } else {
	return;
    }
}
