# Overview #

SNMP Sub-Agent Daemon and SNMP Client to collect and query ZFS information (see slides [http://www.stibor.net/slides/slides_tstibor_27_01_2014.pdf](http://www.stibor.net/slides/slides_tstibor_27_01_2014.pdf))

![zfsnmp.png](https://bitbucket.org/repo/qAb6oB/images/865372622-zfsnmp.png)