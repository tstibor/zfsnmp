/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <t.stibor@gsi.de>
 */

#include <fcntl.h>
#include <signal.h>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include <poolTable.h>
#include "notification.h"

#define VERSION "0.2"
#define PID_LOCK_FILE "/var/run/zfsnmpd.pid"

static int keep_running;
static int pid_fh;

/* HACK: 
   warning: implicit declaration of function ‘init_vacm_vars’ [-Wimplicit-function-declaration] 
   warning: implicit declaration of function ‘init_usmUser’ [-Wimplicit-function-declaration]
*/
extern void init_vacm_vars(void);
extern void init_usmUser(void);

RETSIGTYPE stop_server(int a) {
    keep_running = 0;
}

void init_daemon()
{
    int i; 
    char str_pid[16];

    /* Close (std) descriptors. */
    for (i = getdtablesize(); i >= 0; --i)
        close(i);
    
    if ((chdir("/")) < 0) 
        exit(EXIT_FAILURE);

    pid_fh = open(PID_LOCK_FILE, O_RDWR | O_CREAT, 0640);
    if (pid_fh < 0) {   /* Make sure only one copy exists. */
        snmp_log(LOG_ERR, "error: can not create PID file %s\n", PID_LOCK_FILE);
        exit(EXIT_FAILURE);
    }
    if (lockf(pid_fh, F_TLOCK, 0) < 0) { /* Lock the file. */
        snmp_log(LOG_ERR, "error: can not lock PID file %s\n", PID_LOCK_FILE);
        exit(EXIT_FAILURE);
    }

    /* Write PID to lockfile */
    sprintf(str_pid, "%d\n", getpid());
    ssize_t rc = write(pid_fh, str_pid, strlen(str_pid));
    if (rc == -1) {
	snmp_log(LOG_ERR, "error: can not write PID to lock file %s\n", PID_LOCK_FILE);
	exit(EXIT_FAILURE);
    }

    /* Ignore the following signals. */
    signal(SIGCHLD, SIG_IGN); 
    signal(SIGTSTP, SIG_IGN); 
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);

    /* Catch these signals. */
    signal(SIGTERM, stop_server);
    signal(SIGINT, stop_server);

    snmp_log(LOG_INFO, "info: zfsnmpd daemon started");
}

void finalize_daemon()
{
    int rc;
    rc = close(pid_fh);

    if (rc < 0)
        snmp_log(LOG_WARNING, "warning: cannot close PID file %s", PID_LOCK_FILE);
    rc = remove(PID_LOCK_FILE);

    if (rc < 0)
        snmp_log(LOG_WARNING, "warning: can not remove PID file %s", PID_LOCK_FILE);
}

void usage(const char *prgname)
{
    printf("syntax: %s\n", prgname);
    printf("        -n <num> [optional] sending notifications to trap sink each <num> seconds\n");
    printf("        -f run in foreground\n");
    printf("\nversion %s, written by <t.stibor@gsi.de>, HPC Group at GSI, 2014\n", VERSION);
    exit(1);
}

int main (int argc, char **argv) 
{
    int c;
    int notify_each_sec = 0;
    int agentx_subagent = 1; /* Be a SNMP master agent. */
    int background = 1; /* Run in the background. */
    int syslog = 1; /* Use syslog. */

    if (argc > 4 )
	usage(argv[0]);

    while ((c = getopt (argc, argv, "n:f")) != -1)
	switch (c)
	{
	case 'n':
	    notify_each_sec = atoi(optarg);
	    if (notify_each_sec < 1) {
		printf("error: -n %d is smaller than 1, that does not make sense!\n", notify_each_sec);
		usage(argv[0]);
	    }
	    break;
	case 'f':
	    /* Run in foreground and do not use syslog. */
	    background = 0;
	    syslog = 0;
	    break;
	default:
	    usage(argv[0]);
	    break;
	}

    /* Print log errors to syslog or stderr. */
    if (syslog)
	snmp_enable_calllog();
    else
	snmp_enable_stderrlog();

    /* We're an agentx subagent? */
    if (agentx_subagent) {
	/* Make us a agentx client. */
	netsnmp_ds_set_boolean(NETSNMP_DS_APPLICATION_ID, NETSNMP_DS_AGENT_ROLE, 1);
    }

    /* Run in background, if requested. */
    if (background && netsnmp_daemonize(1, !syslog))
	exit(1);

    /* Create PID lock file. */
    if (background)
	init_daemon();

    /* Initialize TCPIP, if necessary. */
    SOCK_STARTUP;

    /* Initialize the agent library. */
    init_agent("zfsnmpd");

    /* Initialize MIB and fill poolTable entries. */
    init_poolTable();

    /* Initialize vacm/usm access control. */
    if (!agentx_subagent) {
	init_vacm_vars();
	init_usmUser();
    }

    /* zfsnmpd will be used to read zfsnmpd.conf files. Note there
     is a flaw in the SNMP API. We get no return code whether the subagent was started or failed. */
    init_snmp("zfsnmpd");

    /* If we're going to be a snmp master agent, initial the ports */
    if (!agentx_subagent)
	init_master_agent();  /* open the port to listen on (defaults to udp:161) */

    /* In case we recevie a request to stop (kill -TERM or kill -INT) */
    keep_running = 1;

    /* Init notification service. */
    if (notify_each_sec > 0) {
	init_notification(notify_each_sec);
	snmp_log(LOG_INFO, "zfsnmpd is up and running, trap notification will be send every %d sec\n", notify_each_sec);
    } else
	snmp_log(LOG_INFO, "zfsnmpd is up and running, trap notification is deactivated\n");

    while(keep_running) {
	agent_check_and_process(1); /* 0 == don't block */
    }

    /* Close libzfs_handle. */
    close_libzfs();

    /* At shutdown time. */
    snmp_shutdown("zfsnmpd");
    SOCK_CLEANUP;

    /* Remove PID lock file. */
    if (background)
	finalize_daemon();

    return 0;
}

