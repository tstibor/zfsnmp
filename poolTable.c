/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <t.stibor@gsi.de>
 */

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "poolTable.h"

char zfs_pool_name[POOLS_MAX][ZFS_MAXNAMELEN]; /** Array of ZFS pool names.  */
unsigned int n_pools = 0;    /** Total number of ZFS pools.  */
libzfs_handle_t *libzfs_handle = NULL;


/* Needs to be shared by poolTable and notification */
struct poolTable_entry  *poolTable_head;

/* Create a new row in the (unsorted) table. */
struct poolTable_entry *poolTable_createEntry(long poolIndex) 
{
    struct poolTable_entry *entry;

    entry = SNMP_MALLOC_TYPEDEF(struct poolTable_entry);
    if (!entry)
        return NULL;

    entry->poolIndex = poolIndex;
    entry->next = poolTable_head;
    poolTable_head = entry;
    return entry;
}

/* Remove a row from the table. */
void poolTable_removeEntry( struct poolTable_entry *entry ) {
    struct poolTable_entry *ptr, *prev;

    if (!entry)
        return;    /* Nothing to remove */

    for ( ptr  = poolTable_head, prev = NULL;
          ptr != NULL;
          prev = ptr, ptr = ptr->next ) {
        if ( ptr == entry )
            break;
    }
    if ( !ptr )
        return;    /* Can't find it */

    if ( prev == NULL )
        poolTable_head = ptr->next;
    else
        prev->next = ptr->next;

    SNMP_FREE(entry);   /* XXX - release any other internal resources */
}

#if 0
static unsigned long long llu_counter64(const U64 *c64)
{
    unsigned long long llu;
    llu = c64->low;
    llu |= c64->high << 32;

    return llu;
}
#endif

/* Example iterator hook routines - using 'get_next' to do most of the work. */
netsnmp_variable_list *poolTable_get_first_data_point(void **my_loop_context,
						      void **my_data_context,
						      netsnmp_variable_list *put_index_data,
						      netsnmp_iterator_info *mydata)
{
    *my_loop_context = poolTable_head;
    return poolTable_get_next_data_point(my_loop_context, 
					 my_data_context,
					 put_index_data,  mydata );
}

netsnmp_variable_list *poolTable_get_next_data_point(void **my_loop_context,
						     void **my_data_context,
						     netsnmp_variable_list *put_index_data,
						     netsnmp_iterator_info *mydata)
{
    struct poolTable_entry *entry = (struct poolTable_entry *)*my_loop_context;
    netsnmp_variable_list *idx = put_index_data;

    if (entry) {
        snmp_set_var_typed_integer(idx, ASN_INTEGER, entry->poolIndex);
        idx = idx->next_variable;
        *my_data_context = (void *)entry;
        *my_loop_context = (void *)entry->next;
        return put_index_data;
    } else {
        return NULL;
    }
}


/** Handles requests for the poolTable table. */
int poolTable_handler(netsnmp_mib_handler          *handler,
		      netsnmp_handler_registration *reginfo,
		      netsnmp_agent_request_info   *reqinfo,
		      netsnmp_request_info         *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    struct poolTable_entry     *table_entry;

    char result[ZFS_MAXPROPLEN];

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            table_entry = (struct poolTable_entry *)
                              netsnmp_extract_iterator_context(request);
            table_info  =     netsnmp_extract_table_info(request);
    
            switch (table_info->colnum) {
            case COLUMN_POOLINDEX:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->poolIndex);
                break;
            case COLUMN_POOLNAME:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                 (u_char*)table_entry->poolName,
                                          table_entry->poolName_len);
                break;
            case COLUMN_POOLHEALTH:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }

		/* Pool health. */
		if (get_zpool_prop(libzfs_handle, table_entry->poolName, ZPOOL_PROP_HEALTH, result) == ERROR)
		    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
		else {
		    strcpy(table_entry->poolHealth, result);
		    table_entry->poolHealth_len = strlen(result);

		    snmp_set_var_typed_value(request->requestvb, ASN_OCTET_STR,
					     (u_char*)table_entry->poolHealth,
					     table_entry->poolHealth_len);
		}
                break;
            case COLUMN_POOLAVAIL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }

		/* Available space. */
		if (get_zfs_prop(libzfs_handle, table_entry->poolName, ZFS_PROP_AVAILABLE, result) == ERROR)
		    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
		else {
		    read64(&table_entry->poolAvail, result);
		    snmp_set_var_typed_value(request->requestvb, ASN_COUNTER64,
					     (u_char *)&table_entry->poolAvail, sizeof(U64));
		}
                break;
            case COLUMN_POOLUSED:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }

		/* Used space. */
		if (get_zfs_prop(libzfs_handle, table_entry->poolName, ZFS_PROP_USED, result) == ERROR)
		    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
		else {
		    read64(&table_entry->poolUsed, result);
		    snmp_set_var_typed_value(request->requestvb, ASN_COUNTER64,
					     (u_char *)&table_entry->poolUsed, sizeof(U64));
		}
                break;
            case COLUMN_POOLTOTAL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }

		/* Total space. */
		if (get_zfs_prop(libzfs_handle, table_entry->poolName, ZFS_PROP_AVAILABLE, result) == ERROR) {
		    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
		    break;
		}
		else {
		    read64(&table_entry->poolAvail, result);
		    memset(result, 0, ZFS_MAXPROPLEN);
		    if (get_zfs_prop(libzfs_handle, table_entry->poolName, ZFS_PROP_USED, result) == ERROR) {
			netsnmp_set_request_error(reqinfo, request,
						  SNMP_NOSUCHINSTANCE);
			break;
		    } else {
			read64(&table_entry->poolUsed, result);
			table_entry->poolTotal.low = table_entry->poolAvail.low;
			table_entry->poolTotal.high = table_entry->poolAvail.high;
			u64Incr(&table_entry->poolTotal, &table_entry->poolUsed);

			snmp_set_var_typed_value(request->requestvb, ASN_COUNTER64,
						 (u_char *)&table_entry->poolTotal, sizeof(U64));
			break;
		    }
		}
                break;
            case COLUMN_POOLCOMPRESSRATIO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }

		/* Compression ratio. */
		if (get_zfs_prop(libzfs_handle, table_entry->poolName, ZFS_PROP_COMPRESSRATIO, result) == ERROR)
		    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
		else {
		    strcpy(table_entry->poolCompressRatio, result);
		    table_entry->poolCompressRatio_len = strlen(result);

		    snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
					      (u_char*)table_entry->poolCompressRatio,
					      table_entry->poolCompressRatio_len);
		}
                break;
            case COLUMN_POOLDEDUPRATIO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }

		/* Deduplication ratio. */
		uint64_t dedupratio;
		if (get_zpool_prop_int(libzfs_handle, table_entry->poolName, 
				       ZPOOL_PROP_DEDUPRATIO, &dedupratio) == ERROR)
		    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
		else {
		    sprintf(result, "%llu.%02llux", (u_longlong_t)(dedupratio / 100), (u_longlong_t)(dedupratio % 100));
		    strcpy(table_entry->poolDedupRatio, result);
		    table_entry->poolDedupRatio_len = strlen(result);

		    snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
					      (u_char*)table_entry->poolDedupRatio,
					      table_entry->poolDedupRatio_len);
		}
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

    }
    return SNMP_ERR_NOERROR;
}

/** Initializes the poolTable module */
void init_poolTable(void)
{
    /* Initialize all the tables we're planning on supporting. */
    initialize_table_poolTable();
}

int init_entries(void)
{
    struct poolTable_entry *entry;
    int rc;

    libzfs_handle = libzfs_init();
    if (libzfs_handle == NULL) {
	printf("Error: libzfs_init() cannot open libzfs handle\n");
	return ERROR;
    }
    libzfs_print_on_error(libzfs_handle, B_TRUE);

    /* Discover ZFS pools and store pool name in
       zfs_pool_name[0], zfs_pool_name[1], ...
       The number of pools is stored in n_pools. */
    rc = discover_pool_names(libzfs_handle, &zfs_pool_name[0], &n_pools);
    if (rc == ERROR)
	return rc;

    char result[ZFS_MAXPROPLEN];
    for (unsigned int i = 0; i < n_pools; i++) {
	entry = poolTable_createEntry(i+1);

	/* Pool name */
	strcpy(entry->poolName, zfs_pool_name[i]);
	entry->poolName_len = strlen(zfs_pool_name[i]);

	/* Pool health. */
	if (get_zpool_prop(libzfs_handle, zfs_pool_name[i], ZPOOL_PROP_HEALTH, result) == ERROR)
	    result[0] = '\0';
	strcpy(entry->poolHealth, result);
	entry->poolHealth_len = strlen(result);
	memset(result, 0, ZFS_MAXPROPLEN);

	/* Available space. */
	if (get_zfs_prop(libzfs_handle, zfs_pool_name[i], ZFS_PROP_AVAILABLE, result) == ERROR)
	    entry->poolAvail.low = entry->poolAvail.high = 0;
	else
	    read64(&entry->poolAvail, result);
	memset(result, 0, ZFS_MAXPROPLEN);

	/* Used space. */
	if (get_zfs_prop(libzfs_handle, zfs_pool_name[i], ZFS_PROP_USED, result) == ERROR)
	    entry->poolUsed.low = entry->poolUsed.high = 0;
	else
	    read64(&entry->poolUsed, result);
	memset(result, 0, ZFS_MAXPROPLEN);

	/* Total space. */
	entry->poolTotal.low = entry->poolAvail.low;
	entry->poolTotal.high = entry->poolAvail.high;
	u64Incr(&entry->poolTotal, &entry->poolUsed);

	/* Compression ratio. */
	if (get_zfs_prop(libzfs_handle, zfs_pool_name[i], ZFS_PROP_COMPRESSRATIO, result) == ERROR)
	    result[0] = '\0';
	strcpy(entry->poolCompressRatio, result);
	entry->poolCompressRatio_len = strlen(result);
	memset(result, 0, ZFS_MAXPROPLEN);

	/* Deduplication ratio. */
	uint64_t dedupratio;
	if (get_zpool_prop_int(libzfs_handle, zfs_pool_name[i], ZPOOL_PROP_DEDUPRATIO, &dedupratio) == ERROR)
	    dedupratio = 0;
	sprintf(result, "%llu.%02llux", (u_longlong_t)(dedupratio / 100), (u_longlong_t)(dedupratio % 100));
	strcpy(entry->poolDedupRatio, result);
	entry->poolDedupRatio_len = strlen(result);
	memset(result, 0, ZFS_MAXPROPLEN);
    }

    return SUCCESS;
}

/** Initialize the poolTable table by defining its contents and how it's structured. */
void initialize_table_poolTable(void)
{
    static oid poolTable_oid[] = {1,3,6,1,4,1,43231,1,1,1,1};
    size_t poolTable_oid_len   = OID_LENGTH(poolTable_oid);
    netsnmp_handler_registration    *reg;
    netsnmp_iterator_info           *iinfo;
    netsnmp_table_registration_info *table_info;

    reg = netsnmp_create_handler_registration(
              "poolTable",     poolTable_handler,
              poolTable_oid, poolTable_oid_len,
              HANDLER_CAN_RONLY
              );

    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: poolIndex */
                           0);
    table_info->min_column = COLUMN_POOLINDEX;
    table_info->max_column = COLUMN_POOLDEDUPRATIO;
    
    iinfo = SNMP_MALLOC_TYPEDEF( netsnmp_iterator_info );
    iinfo->get_first_data_point = poolTable_get_first_data_point;
    iinfo->get_next_data_point  = poolTable_get_next_data_point;
    iinfo->table_reginfo        = table_info;
    
    netsnmp_register_table_iterator( reg, iinfo );

    /* Initialise the contents of the table here */
    init_entries();
}

/* Return the number of pools. */
inline unsigned int get_n_pools()
{
    return n_pools;
}

/* Required for trap notification. */
inline void update_health_status(struct poolTable_entry *table_entry)
{
    char result[ZFS_MAXPROPLEN];

    if (get_zpool_prop(libzfs_handle, table_entry->poolName, ZPOOL_PROP_HEALTH, result) == ERROR) {
	strcpy(result, "(unknown)");
	strcpy(table_entry->poolHealth, result);
	table_entry->poolHealth_len = strlen(result);
    } else {
	strcpy(table_entry->poolHealth, result);
	table_entry->poolHealth_len = strlen(result);
    }
}

void close_libzfs()
{
    libzfs_fini(libzfs_handle);
}
