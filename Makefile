CC = gcc

CFLAGS         = -std=c99 -Wall -O2 # Replace -g (debug flag) with -O2 (optimize flag)
INCLUDES_SNMP  = -I. `net-snmp-config --cflags`
LIB_SNMP       = `net-snmp-config --libs`
LIB_SNMP_AGENT = `net-snmp-config --agent-libs`

INCLUDE_LOCAL  = /usr/local/include
INCLUDES_ZFS   = -I$(INCLUDE_LOCAL)/libzfs -I$(INCLUDE_LOCAL)/libzfs/sys -I$(INCLUDE_LOCAL)/libzfs/linux -I$(INCLUDE_LOCAL)/libspl -DHAVE_IOCTL_IN_UNISTD_H
LIB_ZFS        = -lzfs
LIB_CLOCK      = -lrt

BINARY         = zfsnmpd zfsnmp
DEB_BUILD      = deb-build
MIB_PATH       = /usr/share/mibs/netsnmp # see `net-snmp-config --default-mibdirs`
ZFS-MIB_FILE   = mib/ZFS-MIB

DOXYGEN_DIR    = doc/doxygen

all: $(BINARY)

notification.o: notification.c notification.h poolTable.c poolTable.h
	$(CC) $(CFLAGS) $(INCLUDES_SNMP) $(INCLUDES_ZFS) -c $< -o $@

poolTable.o: poolTable.c poolTable.h
	$(CC) $(CFLAGS) $(INCLUDES_SNMP) $(INCLUDES_ZFS) -c $< -o $@

zfsnmpd.o: zfsnmpd.c
	$(CC) $(CFLAGS) $(INCLUDES_SNMP) $(INCLUDES_ZFS) -c $< -o $@

zfsnmpd: zfsnmpd.o poolTable.o zfs_info.o notification.o
	$(CC) $(CFLAGS) $^ -o $@ $(LIB_SNMP) $(LIB_SNMP_AGENT) $(LIB_ZFS)

zfs_info.o: zfs_info.c zfs_info.h
	$(CC) $(CFLAGS) $(INCLUDES_ZFS) $(INCLUDES_SNMP) -c $< -o $@

zfsnmp.o: zfsnmp.c
	$(CC) $(CFLAGS) $(INCLUDES_SNMP) -c $< -o $@

zfsnmp: zfsnmp.o
	$(CC) $(CFLAGS) $^ -o $@ $(LIB_SNMP) $(LIB_CLOCK)

clean:
	rm -rf $(BINARY) *.o $(DEB_BUILD) $(DOXYGEN_DIR)

doxygen:
	doxygen doc/doxygen.cfg

deb: zfsnmp zfsnmpd
	@mkdir -p $(DEB_BUILD)/zfsnmp/DEBIAN $(DEB_BUILD)/zfsnmp/usr/bin
	@mkdir -p $(DEB_BUILD)/zfsnmpd/etc/default $(DEB_BUILD)/zfsnmpd/etc/init.d
	@mkdir -p $(DEB_BUILD)/zfsnmpd/usr/sbin $(DEB_BUILD)/zfsnmpd/DEBIAN $(DEB_BUILD)/zfsnmpd/$(MIB_PATH)

	@cp debian/control.zfsnmp $(DEB_BUILD)/zfsnmp/DEBIAN/control
	@cp debian/control.zfsnmpd $(DEB_BUILD)/zfsnmpd/DEBIAN/control
	@cp debian/postinst.zfsnmpd $(DEB_BUILD)/zfsnmpd/DEBIAN/postinst
	@cp debian/prerm.zfsnmpd $(DEB_BUILD)/zfsnmpd/DEBIAN/prerm
	@cp debian/zfsnmpd.rc $(DEB_BUILD)/zfsnmpd/etc/init.d/zfsnmpd
	@cp debian/default.zfsnmpd $(DEB_BUILD)/zfsnmpd/etc/default/zfsnmpd

	@chmod 755 $(DEB_BUILD)/zfsnmpd/DEBIAN/postinst $(DEB_BUILD)/zfsnmpd/DEBIAN/prerm $(DEB_BUILD)/zfsnmpd/etc/init.d/zfsnmpd

	@cp zfsnmp $(DEB_BUILD)/zfsnmp/usr/bin
	@cp zfsnmpd $(DEB_BUILD)/zfsnmpd/usr/sbin
	@cp $(ZFS-MIB_FILE) $(DEB_BUILD)/zfsnmpd/$(MIB_PATH)

	@sed -i '/Depends/aInstalled-Size: '"`du --exclude='DEBIAN' -ks deb-build/zfsnmp | cut -f 1`"'' $(DEB_BUILD)/zfsnmp/DEBIAN/control
	@sed -i '/Depends/aInstalled-Size: '"`du --exclude='DEBIAN' -ks deb-build/zfsnmpd | cut -f 1`"'' $(DEB_BUILD)/zfsnmpd/DEBIAN/control

	@find $(DEB_BUILD)/zfsnmpd/ -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' | xargs md5sum | sed -e 's/deb-build\/zfsnmpd\///' > $(DEB_BUILD)/zfsnmpd/DEBIAN/md5sums
	@find $(DEB_BUILD)/zfsnmp/ -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' | xargs md5sum | sed -e 's/deb-build\/zfsnmp\///' > $(DEB_BUILD)/zfsnmp/DEBIAN/md5sums

	fakeroot dpkg-deb -b $(DEB_BUILD)/zfsnmpd $(DEB_BUILD) && fakeroot dpkg-deb -b $(DEB_BUILD)/zfsnmp $(DEB_BUILD)

backup:
	/bin/tar jcf - . | /usr/bin/ssh tstibor@lx-pool.gsi.de "/bin/cat > /u/tstibor/backup/backup_zfsnmp_stibor_`/bin/date +"%u"`.tar.bz2"
